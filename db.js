const bcrypt=require('bcrypt')

module.exports.checkAuth=function(loginDetail,con,callback){
    var query='SELECT * FROM users WHERE username=?;';
    
    var params=[loginDetail.username];
    con.query(query,params,function(error,user,fields){
        if(user.length>0){
            bcrypt.compare(loginDetail.pwd, user[0].pwd, function(err, res) {
                if(res==true){
                    callback(true,user[0]);
                }else{
                    callback(false,user);
                }
            });
        }else{
            callback(false,user);
        }
    })
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max))+1;
}


module.exports.addUser=function(userDetail,con,callback){
    var query='INSERT INTO users(username,pwd,usertype) VALUES(?,?,?);';
    
    bcrypt.hash(userDetail.pwd, getRandomInt(6), function(err, hashpwd) {
        var params=[userDetail.username,hashpwd,userDetail.usertype];
        con.query(query,params,function(error,results,fields){
            if(error){
                callback(false,results);
            }else{
                callback(true,results);
            }
        })
    });
}