var mysql      = require('mysql');

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'sail',
  port     : '8080'
});

connection.connect();

module.exports = connection;