CREATE DATABASE sail;
CREATE TABLE users(
	username varchar(256) PRIMARY KEY,
    pwd varchar(256),
    usertype varchar(50)
);