$('#showpwd').on('click',()=>{
    if($('#showpwd').is(":checked")){
        $('#pwd').attr('type','text');
    }else{
        $('#pwd').attr('type','password');
    }
})

const {ipcRenderer}=require('electron')

$('#signupBtn').on('click',()=>{
    console.log('pressed');
    signupDetail={
        username: $('#username').val(),
        pwd: $('#pwd').val(),
        usertype: $('#usertype').val()
    }
    ipcRenderer.send('signupUser',signupDetail);
})