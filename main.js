const {app, BrowserWindow, ipcMain} = require('electron')

require('electron-reload')(__dirname)

let mainWindow

var con=require('./connection.js')
var db=require('./db.js')

function createWindow() {
  mainWindow = new BrowserWindow({ width: 1200, height: 800, minWidth: 400, minHeight: 200, webPreferences: { nodeIntegration: true }})
  // mainWindow.loadFile('admin/signup.html') //uncomment this for first time signup and comment next line
  mainWindow.loadFile('login.html')
  // mainWindow.webContents.openDevTools()
  mainWindow.on('closed', function () { mainWindow = null })
}

app.on('ready',createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  if (mainWindow === null) createWindow()
})









// 
// 
// ADD CHANNELS BELOW THIS
// 
// 

ipcMain.on('loginInit',(e,loginDetail)=>{
  db.checkAuth(loginDetail,con,(status,user)=>{
    if(status){
      console.log('login successful');
      if(user.usertype=='admin'){
        mainWindow.loadFile('admin/admin.html')
      }else if(user.usertype=='other'){
        // load file according to you
      }
    }else{
      console.log('login unsuccessful');
    }
  })
})

ipcMain.on('signupUser',(e,signupDetail)=>{
  db.addUser(signupDetail,con,(status,results)=>{
    if(status){
      console.log('success');
    }else{
      console.log('fail');
    }
  })
})

ipcMain.on('redirectSignup',(e,pressed)=>{
  if(pressed){
    mainWindow.loadFile('admin/signup.html');
  }
})

// 
// 
// ADD CHANNELS ABOVE THIS
// 
// 
